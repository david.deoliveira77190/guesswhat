<?php

namespace App\Tests\Core;

use PHPUnit\Framework\TestCase;
use App\Core\Card;

class CardTest extends TestCase
{

  public function testName()
  {
    $card = new Card('As', 'Trèfle');
    $this->assertEquals('As', $card->getName());
  }

  public function testSort()
  {
    $cards = [];

    $card = new Card('As', 'Trèfle');
    $cards[] = $card;
    $card = new Card('2', 'Pique');
    $cards[] = $card;

    // vérifie que la première carte est bien un As
    $this->assertEquals('As', $cards[0]->getName());

    // trie le tableau $cards, en utilisant la fonction de comparaison Card::cmp
    // rem : la syntaxe n'est pas intuitive, on doit passer
    // le nom complet de la classe et le nom d'une méthode de comparaison.
    // (voir https://www.php.net/manual/fr/function.usort.php)
    usort($cards,  array("App\Core\Card", "cmp"));

    // vérifie que le tableau $cards a bien été modifié par usort
    // dans la table ASCII, les chiffres sont placés avant les lettres de l'alphabet
    $this->assertEquals('2', $cards[0]->getName());
  }

  public function testColor()
  {
      $card = new Card('As', 'Trèfle');
      $cards[] = $card;
      $this->assertEquals('Trèfle', $cards[0]->getColor());
  }

  public function testCmp()
  {
      // 0 si $o1 et $o2 sont considérés comme égaux
      $o1 = new Card('As', 'Trèfle');
      $o2 = new Card('As', 'Trèfle');
      $this->assertEquals('0', Card::cmp($o1, $o2));

      // -1 si $o1 est considéré inférieur à $o2
      $o1 = new Card('As', 'Trèfle');
      $o2 = new Card('As', 'pique');
      $this->assertEquals('-1', Card::cmp($o1, $o2));

      // +1 si $o1 est considéré supérieur à $o2
      $o1 = new Card('As', 'pique');
      $o2 = new Card('valet', 'coeur');
      $this->assertEquals('+1', Card::cmp($o1, $o2));

      $o1 = new Card('3', 'pique');
      $o2 = new Card('2', 'trèfle');
      $this->assertEquals('-1', Card::cmp($o2, $o1));

      $o1 = new Card('2', 'pique');
      $o2 = new Card('3', 'pique');
      $this->assertEquals('-1', Card::cmp($o1, $o2));
  }


    public function testToString()
  {
     $card = new Card('As', 'Caraux');
     $this->assertEquals('As', $card->toString());
  }

}
