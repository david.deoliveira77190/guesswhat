<?php


namespace App\Tests\Core;

use App\Core\Card;
use App\Core\Guess;
use PHPUnit\Framework\TestCase;

class GuessTest extends TestCase
{

    public function testVerifyAndStats()
    {
        // Test Verify
        // carte à trouver == Roi -> Coeur

        $guess1 = new Guess(52); // sans aide (par défaut a false)
        $this->assertEquals(0, $guess1->verify(new Card("7", "Pique")));

        // Test stats() pour $guess2
        $this->assertEquals("Submission(s) : 1", $guess1->stats());

        // -------------------------------------------------------------------------------------------

        $guess2 = new Guess(52, true); // avec aide

        // +1 lorsqu'il soumet une carte plus grande que celle à trouver.
        $this->assertEquals(+1, $guess2->verify(new Card("as", "Coeur")));

        // -1 lorsqu'il soumet une carte plus petite que celle à trouver.
        $this->assertEquals(-1, $guess2->verify(new Card("7", "Coeur")));

        // 1 quand c'est la bonne carte
        $this->assertEquals(1, $guess2->verify(new Card("Roi", "Coeur")));

        // Test stats() pour $guess2
        $this->assertEquals("Submission(s) : 3", $guess2->stats());
    }

}
