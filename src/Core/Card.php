<?php

namespace App\Core;



/**
 * Class Card : Définition d'une carte à jouer
 * @package App\Core
 */
class Card
{
    /**
     * @var $name string nom de la carte, comme par exemples 'As' '2' 'Reine'
     */
    private $name;

    /**
     * @var $color string couleur de la carte, par exemples 'Pique', 'Carreau'
     */
    private $color;

    /**
     * Card constructor.
     * @param string $name
     * @param string $color
     */
    public function __construct(string $name, string $color)
    {
        $this->name = strtolower($name);
        $this->color = strtolower($color);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor(string $color): void
    {
        $this->color = $color;
    }

    /**
     * @return string
     */
    public function toString(): string
    {
        return $this->name . " de " . $this->color;
    }

    /**
     * @return string[]
     */
    public static function cardNames(): array
    {
        return ["as", "roi", "dame", "valet", "10", "9",
            "8", "7", "6", "5", "4", "3", "2"];
    }

    /**
     * @return string[]
     */
    public static function cardColors(): array
    {
        return ["pique", "carreau", "coeur", "trèfle"];
    }

    /** définir une relation d'ordre entre instance de Card.
     *  Remarque : cette méthode n'est pas de portée d'instance
     *
     * @see https://www.php.net/manual/fr/function.usort.php
     *
     * @param $o1 Card
     * @param $o2 Card
     * @return int
     *

     * <ul>
     *  <li> 0 si $o1 et $o2 sont considérés comme égaux </li>
     *  li> -1 si $o1 est considéré inférieur à $o2</li>
     *  <li> +1 si $o1 est considéré supérieur à $o2</li>
     * </ul>
     *  *  Le classement  des cartes
     *  > Carreau > Coeur >  >  Pique > Trèfle
     *
     */
    public static function cmp(Card $o1, Card $o2): int
    {
        $o1Name = $o1->name;
        $o2Name = $o2->name;

        $o1Color = $o1->color;
        $o2Color = $o2->color;

        if ($o1Name == $o2Name && $o1Color == $o2Color) {
            return 0;
        }

        $cardNamesNumber = ["as" => 12, "roi" => 11, "dame" => 10, "valet" => 9, "10" => 8, "9" => 7, "8" => 6,
            "7" => 5, "6" => 4, "5" => 3, "4" => 2, "3" => 1, "2" => 0];
        $cardColorsNumber = [" Carreau" => 3, "Coeur" => 2, "Pique " => 1, "Trèfle" => 0];

        if ($cardColorsNumber[$o1Color] > $cardColorsNumber[$o2Color]) {
            if ($cardNamesNumber[$o1Name] == $cardNamesNumber[$o2Name]) {
                return +1;
            }
            return ($cardNamesNumber[$o1Name] > $cardNamesNumber[$o2Name]) ? +1 : -1;
        } else {
            if ($cardNamesNumber[$o1Name] == $cardNamesNumber[$o2Name]) {
                return -1;
            }
            return ($cardNamesNumber[$o1Name] > $cardNamesNumber[$o2Name]) ? +1 : -1;
        }

    }



}

{

}