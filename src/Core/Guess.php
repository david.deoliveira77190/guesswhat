<?php

namespace App\Core;

/**
 * Class Guess : la logique du jeu.
 * @package App\Core
 */
class Guess
{
    /**
     * @var $game int name of game
     */
    private $game;

    /**
     * @var $help bool Si le joueur active l'aide ou non
     */
    private $help;

    /**
     * @var $cards array a array of Cards
     */
    private $cards;

    /**
     * @var $selectedCard Card This is the card to be guessed by the player
     */
    private $selectedCard;

    /**
     * @var $submissions int nombre de fois où le joueur a soumis une carte
     */
    private $submissions;

    /**
     * Guess constructor.
     * @param int $game choix du jeu de cartes
     * @param bool $help Demande d'aide
     */
    public function __construct(int $game, bool $help = false)
    {
        $this->game = $game;
        $this->help = $help;
        if ($game == 52) {
            $this->cards = Card::gameOf52Cards();
        } else {
            $this->cards = Card::gameOf52Cards();
        }
        $this->selectedCard = new Card("roi", "pique"); // $this->cards->get(mt_rand(0, $this->cards->count()));
        $this->submissions = 0;
    }

    /**
     * @return int
     */
    public function getGame(): int
    {
        return $this->game;
    }

    /**
     * @param int $game
     */
    public function setGame(int $game): void
    {
        $this->game = $game;
    }

    /**
     * @return array
     */
    public function getCards(): array
    {
        return $this->cards;
    }

    /**
     * @param array $cards
     */
    public function setCards(array $cards): void
    {
        $this->cards = $cards;
    }

    /**
     * @return Card
     */
    public function getSelectedCard(): Card
    {
        return $this->selectedCard;
    }

    /**
     * @param Card $selectedCard
     */
    public function setSelectedCard(Card $selectedCard): void
    {
        $this->selectedCard = $selectedCard;
    }

    /**
     * @return int
     */
    public function getSubmissions(): int
    {
        return $this->submissions;
    }

    /**
     * @param int $submissions
     */
    public function setSubmissions(int $submissions): void
    {
        $this->submissions = $submissions;
    }

    /**
     * @param $card Card la carte soumis
     * @return int
     *
     * O si ce n'est pas la bonne carte
     * 1 si c'est la bonne carte
     *
     * Si l'aide est activé :
     * -1 si la carte qu'il a soumise est plus petite que celle à deviner.
     * +1 si la carte qu'il a soumise est plus grande que celle à deviner.
     */
    public function verify(Card $card): int
    {
        $this->submissions++;

        if ($card === $this->selectedCard) {
            return 1;
        } elseif ($card !== $this->selectedCard && $this->help == true) {
            // Utilisation de la méthode static compare (Card)
            return Card::cmp($card, $this->selectedCard);
        } else {
            return 0;
        }
    }

    /**
     * @return string retourne des éléments d'analyse de la partie
     */
    public function stats(): string
    {
        return "Submission(s) : " . $this->getSubmissions();
    }

}



